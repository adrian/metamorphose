for file in hd/galerie* ; do
    f="$(basename "$file")"
    convert -define webp:size=2000x720 \
        -auto-orient \
        -thumbnail 1000x360 \
        -unsharp 0x.5 \
        "./$file" \
        "./miniature/${f%.jpg}.webp"
done

