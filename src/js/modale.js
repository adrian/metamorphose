var numberOfImages = 9;
var currentImageNumber = null;

function viewPicture (number) {
    document.getElementById('modale').style.display = 'flex';
    document.getElementById('body').style.overflow = 'hidden';
    getPicture(number);
}

function getPicture (number) {
	currentImageNumber = number;
	document.getElementById('modale_wide_view').src = 'img/hd/galerie' + number + '.jpg';
}

function closeModalView () {
    document.getElementById('modale').style.display = 'none';
    document.getElementById('body').style.overflow = 'auto';
	document.getElementById('modale_wide_view').src = '';
}

function leftModalView () {
    getPicture((currentImageNumber - 1 + numberOfImages) % numberOfImages);
}

function rightModalView () {
    getPicture((currentImageNumber + 1) % numberOfImages);
}

/* Close modale when pressing escape */
// TODO
/*
window.onload = function () {
    document.getElementById('body').addEventListener('keypress', function(e) {
        console.log(e)
        if(e.key == "Escape"){
            closeModalView()
        }
    });
}
*/
